import { createRouter, createWebHistory } from 'vue-router'
import Projects from '../views/Projects.vue'
import About from '../views/About.vue'

const routes = [
  {
    path: '/projects',
    name: 'Projects',
    component: Projects
  },
  {
    path: '/about',
    name: 'About',
    component: About
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
